# Pod Tourney #

### What is this repository for? ###

This code demonstrates two different approaches to the problem of scheduling "pods" (players) in a tournament under the following assumptions:

* games are played sequentially (one at a time), with each game pitting a team in white jerseys against a team in black jerseys;
* each pod teams with every other pod exactly once and plays against every other pod exactly twice;
* no pod can play consecutive games in different color jerseys; and
* the objective is to minimize the aggregate number of jersey changes.

The problem was originally [posted on Mathematics StackExchange](https://math.stackexchange.com/questions/4370828/tournament-schedule-assignment-via-convex-optimization). The Java code here implements both an integer programming model (solved by CPLEX) and a constraint programming model (solved by CP Optimizer).

The code was developed using a beta version of CPLEX Studio 22.1 but will run with at least some earlier versions.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

