package problem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Problem contains an instance of the tourney problem.
 *
 * The problem instance contains the following embedded assumptions:
 * - every pod is teamed exactly once with every other pod;
 * - every pod faces every other pod exactly twice as opponents;
 * - only one game (four pods) occurs at a time;
 * - one team wears white, the other wears black; and
 * - no pod plays in consecutive time slots with different jersey colors.
 *
 * Each game has two slots, indexed s (0 ... nGames - 1) and s + nGames.
 * The team in the lower indexed slot wears white and the team in the higher
 * indexed slot wears black.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  /** MAXRUN is the maximum number of consecutive games in which any pod
   * can be forced to play. */
  public static final int MAXRUN = 3;    // max consecutive games for any pod
  private static final int PLAYERS = 4;  // number of pods per game
  private final int nPods;   // number of pods
  private final int nGames;  // number of games to schedule
  private final int nSlots;  // number of schedule slots (two per game)
  private final ArrayList<Team> allTeams;  // list of all possible teams
  private final HashMap<Integer, Set<Integer>> member;  // maps pod to teams

  /**
   * Constructor.
   * @param size the problem size (number of pods
   */
  public Problem(final int size) {
    nPods = size;
    nGames = nPods * (nPods - 1) / PLAYERS;
    nSlots = 2 * nGames;
    // Make a list of all teams.
    allTeams = new ArrayList<>();
    for (int i = 0; i < nPods; i++) {
      for (int j = i + 1; j < nPods; j++) {
        allTeams.add(new Team(i, j));
      }
    }
    // Map pod indices to indices of teams containing the pod.
    member = new HashMap<>();
    for (int t = 0; t < allTeams.size(); t++) {
      for (int i : allTeams.get(t).members()) {
        if (member.containsKey(i)) {
          member.get(i).add(t);
        } else {
          HashSet<Integer> s = new HashSet<>();
          s.add(t);
          member.put(i, s);
        }
      }
    }
  }

  /**
   * Gets the number of pods.
   * @return the number of pods
   */
  public int getNPods() {
    return nPods;
  }

  /**
   * Gets the number of games.
   * @return the number of games
   */
  public int getNGames() {
    return nGames;
  }

  /**
   * Gets the number of game slots.
   * @return the number of slots
   */
  public int getNSlots() {
    return nSlots;
  }

  /**
   * Gets the number of teams.
   * @return the team count
   */
  public int getNTeams() {
    return allTeams.size();
  }

  /**
   * Gets a team by index number.
   * @param index the index of the team
   * @return the team
   */
  public Team getTeam(final int index) {
    return allTeams.get(index);
  }

  /**
   * Gets an iterator over the list of teams.
   * @return an iterator over all teams
   */
  public Iterator<Team> teamIterator() {
    return allTeams.iterator();
  }

  /**
   * Gets the set of teams containing a pod.
   * @param index the index of the pod
   * @return the set of indices of teams to which the pod belongs
   */
  public Set<Integer> getTeams(final int index) {
    return member.get(index);
  }

  /**
   * Gets the index of the slot opposite a given slot (same game, other jersey
   * color).
   * @param slot the input slot
   * @return the opposing slot
   */
  public int oppositeSlot(final int slot) {
    return (slot < nGames) ? slot + nGames : slot - nGames;
  }

  /**
   * Produces a label suitable for a CPLEX/CP Optimizer object from a list
   * of ingredients.
   * @param prefix a text prefix for the label
   * @param suffix a text suffix for the label (optional)
   * @param indices one or more indices to include in the label
   * @return the label string
   */
  public String label(final String prefix, final String suffix,
                      final int... indices) {
    StringBuilder sb = new StringBuilder(prefix);
    for (int i : indices) {
      sb.append("_").append(i);
    }
    if (suffix != null && !suffix.isBlank()) {
      sb.append("_").append(suffix);
    }
    return sb.toString();
  }

  /**
   * Converts a slot index into the corresponding game index.
   * @param slot the slot index
   * @return the game index
   */
  public int slotToGame(final int slot) {
    return slot % nGames;
  }

  /**
   * Converts a slot index to the corresponding jersey color.
   * @param slot the slot index
   * @return the jersey color (0 = white for the first half of all slots,
   * 1 = black for the second half)
   */
  public int slotToColor(final int slot) {
    return slot / nGames;
  }

  /**
   * Gets the slot for a particular side of a particular game.
   * @param game the game index
   * @param color the jersey color
   * @return the slot index
   */
  public int getSlot(final int game, final int color) {
    return game + nGames * color;
  }
}
