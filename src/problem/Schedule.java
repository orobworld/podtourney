package problem;

import java.util.Arrays;

/**
 * Schedule contains a schedule of all games.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Schedule {
  private final Problem problem;    // the problem instance
  private final int[][] schedule;   // schedule[i][j] is the index of the team
                                    // in game i wearing white (j=0)
                                    // or black (j=1)
  private final int nGames;         // number of games in the schedule

  /**
   * Constructs an empty schedule.
   * @param prob the problem being solved
   */
  public Schedule(final Problem prob) {
    problem = prob;
    nGames = problem.getNGames();
    schedule = new int[nGames][2];
  }

  /**
   * Assigns a team to a game and jersey color.
   * @param team the index of the team
   * @param game the index of the game
   * @param color the jersey color (0 or 1)
   */
  public void assign(final int team, final int game, final int color) {
    schedule[game][color] = team;
  }

  /**
   * Generates a string representation of the schedule.
   * @return a string containing the complete schedule
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Tournament schedule (the team on the left wearing white):\n");
    for (int i = 0; i < schedule.length; i++) {
      Team w = problem.getTeam(schedule[i][0]);
      Team b = problem.getTeam(schedule[i][1]);
      sb.append("Game ").append(i + 1).append(": ").append(w).append(" vs. ")
        .append(b).append("\n");
    }
    return sb.toString();
  }

  /**
   * Gets the index of the team in a particular slot.
   * @param game the index of the game
   * @param color the jersey color (0 or 1)
   * @return the index of the team wearing the specified color
   * in the specified game
   */
  public int getTeam(final int game, final int color) {
    return schedule[game][color];
  }

  /**
   * Checks whether a pod is playing in a game.
   * @param pod the index of the pod
   * @param game the index of the game
   * @return true if the pod is in the game
   */
  public boolean isPlaying(final int pod, final int game) {
    for (int c = 0; c < 2; c++) {
      Team t = problem.getTeam(schedule[game][c]);
      if (t.contains(pod)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets the colors worn by a pod.
   * @param pod the index of the pod
   * @return the vector of colors worn in each game
   */
  public int[] getColors(final int pod) {
    int[] colors = new int[nGames];
    // Set all colors to -1 (unknown) to start.
    Arrays.fill(colors, -1);
    // Set the jersey color in all games in which the pod is playing, and
    // track the first game in which it appears.
    int first = nGames;
    for (int g = 0; g < nGames; g++) {
      for (int c = 0; c < 2; c++) {
        Team t = problem.getTeam(schedule[g][c]);
        if (t.contains(pod)) {
          colors[g] = c;
          first = Math.min(first, g);
          break;
        }
      }
    }
    // For any games before the first appearance, set the color to the color
    // in the first appearance.
    int current = colors[first];
    for (int g = 0; g < first; g++) {
      colors[g] = current;
    }
    // Working forward from the first appearance, change any missing values
    // to the last color worn.
    for (int g = first + 1; g < nGames; g++) {
      if (colors[g] < 0) {
        // Missing value -- set to current color.
        colors[g] = current;
      } else {
        // Know color -- make it the current color.
        current = colors[g];
      }
    }
    return colors;
  }

  /**
   * Tests whether two pods are opponents in a game.
   * @param pod1 the index of the first pod
   * @param pod2 the index of the second pod
   * @param game the index of the game
   * @return true if both pods are in the game, on opposing teams
   */
  public boolean areOpponents(final int pod1, final int pod2, final int game) {
    Team w = problem.getTeam(schedule[game][0]);
    Team b = problem.getTeam(schedule[game][1]);
    return ((w.contains(pod1) && b.contains(pod2))
            || (w.contains(pod2) && b.contains(pod1)));
  }
}
