package problem;

/**
 * Team holds the indices of two pods playing as a team.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public record Team(int first, int second) {
  
  /**
   * Generates a string representation of the team.
   * Pod indices are incremented by 1 so that they range from 1 to nPods.
   * @return a string describing the team
   */
  @Override
  public String toString() {
    return (first + 1) + " & " + (second + 1);
  }

  /**
   * Gets the indices of the team members.
   * @return an array containing the indices of the team members
   */
  public int[] members() {
    return new int[] {first, second};
  }

  /**
   * Tests whether a pod is on the team.
   * @param pod the pod to look for
   * @return true if the pod belongs to the team
   */
  public boolean contains(final int pod) {
    return (first == pod) || (second == pod);
  }
}
