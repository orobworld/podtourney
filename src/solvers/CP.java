package solvers;

import ilog.concert.IloConstraint;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloSolution;
import ilog.concert.cppimpl.IloAlgorithm.Status;
import ilog.cp.IloCP;
import ilog.cp.IloSearchPhase;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.IntStream;
import problem.Problem;
import problem.Schedule;
import problem.Team;

/**
 * CP implements a constraint programming model for the tourney problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CP implements Solver {
  private static final int LOGPERIOD = 500000;  // log output period

  private final Problem problem;   // parent problem
  private final int nPods;         // number of pods
  private final int nTeams;        // the number of teams
  private final int nSlots;        // number of schedule slots
  private final int nGames;        // number of games played

  // CP Optimizer objects.
  private final IloCP cp;               // the solver instance
  private final IloIntVar[] slot;       // slot[t] is the slot in which team t
                                        // is scheduled
  private final IloIntVar[][] playing;  // playing[p][g] is 1 if pod p is
                                        // playing in game g
  private final IloIntVar[][] color;    // color[p][g] is the color worn by
                                        // pod p during game g (playing or not)
  private final IloIntVar[][] change;   // change[p][g] is 1 if pod p changes
                                        // color for game g
  private final IloIntVar[][][] opponents;
                                        // opponents[p][q][g] = 1 if pods p
                                        // and q > p are in opposite slots of
                                        // game g

  /**
   * Constructor.
   * @param prob the problem to solve
   * @throws IloException if CP Optimizer cannot build the model
   */
  public CP(final Problem prob) throws IloException {
    problem = prob;
    nPods = problem.getNPods();
    nTeams = problem.getNTeams();
    nSlots = problem.getNSlots();
    nGames = problem.getNGames();
    // Set up domain arrays.
    int[] slots = IntStream.range(0, nSlots).toArray();
    // Initialize the solver.
    cp = new IloCP();
    // Create the variables.
    slot = new IloIntVar[nTeams];
    playing = new IloIntVar[nPods][nGames];
    color = new IloIntVar[nPods][nGames];
    change = new IloIntVar[nPods][nGames];
    opponents = new IloIntVar[nPods][nPods][nGames];
    for (int t = 0; t < nTeams; t++) {
      slot[t] = cp.intVar(slots, problem.label("slot", "", t));
    }
    for (int p = 0; p < nPods; p++) {
      for (int g = 0; g < nGames; g++) {
        playing[p][g] = cp.boolVar(problem.label("playing", "", p, g));
        color[p][g] = cp.boolVar(problem.label("color", "", p, g));
        change[p][g] = cp.boolVar(problem.label("change", "", p, g));
        for (int q = p + 1; q < nPods; q++) {
          opponents[p][q][g] =
            cp.boolVar(problem.label("opponents", "", p, q, g));
        }
      }
    }
    // Add constraints.
    // Two teams cannot occupy the same schedule slot.
    cp.add(cp.allDiff(slot));
    // Occupying a slot means a pod is playing and dictates the pod's
    // jersey color.
    for (int t = 0; t < nTeams; t++) {
      Team team = problem.getTeam(t);
      for (int s = 0; s < nSlots; s++) {
        int c = problem.slotToColor(s);
        int g = problem.slotToGame(s);
        // Occupying slot implies both pods in team are playing.
        cp.add(cp.imply(cp.eq(slot[t], s),
                        cp.and(cp.eq(playing[team.first()][g], 1),
                               cp.eq(playing[team.second()][g], 1))));
        // Occupying slot implies jersey colors for both pods in team.
        cp.add(cp.imply(cp.eq(slot[t], s),
                        cp.and(cp.eq(color[team.first()][g], c),
                               cp.eq(color[team.second()][g], c))));
      }
    }
    // Jersey color remains constant unless a change is recorded.
    for (int p = 0; p < nPods; p++) {
      for (int g = 1; g < nGames; g++) {
        cp.add(cp.equiv(cp.neq(color[p][g - 1], color[p][g]),
                        cp.eq(change[p][g], 1)));
      }
    }
    // Playing in consecutive games implies no change in color.
    for (int p = 0; p < nPods; p++) {
      for (int g = 1; g < nGames; g++) {
        cp.add(cp.imply(cp.and(cp.eq(playing[p][g - 1], 1),
                               cp.eq(playing[p][g], 1)),
                        cp.eq(change[p][g], 0)));
      }
    }
    // Two pods are opponents if and only if they are playing in the same game
    // with different colors.
    for (int p = 0; p < nPods; p++) {
      for (int q = p + 1; q < nPods; q++) {
        for (int g = 0; g < nGames; g++) {
          cp.add(cp.equiv(cp.eq(opponents[p][q][g], 1),
                          cp.and(new IloConstraint[] {
                            cp.eq(playing[p][g], 1),
                            cp.eq(playing[q][g], 1),
                            cp.neq(color[p][g], color[q][g])})));
        }
      }
    }
    // Every pair of pods faces each other exactly twice.
    for (int p = 0; p < nPods; p++) {
      for (int q = p + 1; q < nPods; q++) {
        cp.addEq(cp.sum(opponents[p][q]), 2);
      }
    }
    // No pod can play in more than MAXRUN consecutive games.
    for (int p = 0; p < nPods; p++) {
      for (int g = Problem.MAXRUN; g < nGames; g++) {
        // Pod p can make at most MAXRUN appearances in the MAXRUN + 1 games
        // ending with game g.
        IloIntVar[] x =
          Arrays.copyOfRange(playing[p], g - Problem.MAXRUN, g + 1);
        cp.addLe(cp.sum(x), Problem.MAXRUN);
      }
    }
    // The objective is to minimize the number of jersey changes.
    IloLinearNumExpr obj = cp.linearNumExpr();
    for (int p = 0; p < nPods; p++) {
      for (int g = 0; g < nGames; g++) {
        obj.addTerm(1.0, change[p][g]);
      }
    }
    cp.addMinimize(obj);
    // Throttle the output a bit.
    cp.setParameter(IloCP.IntParam.LogPeriod, LOGPERIOD);
  }

  /**
   * Closes the solver instance.
   */
  @Override
  public void close() {
    cp.end();
  }

  /**
   * Gets the final schedule (if one exists).
   * @return the final schedule
   * @throws IloException if the schedule cannot be obtained
   */
  @Override
  public Optional<Schedule> getSchedule() throws IloException {
    Status status = cp.getStatus();
    if (status == Status.Optimal || status == Status.Feasible) {
      // CPO has a solution. Create a blank schedule.
      Schedule schedule = new Schedule(problem);
      // Translate each slot assignment into entries in the schedule.
      for (int t = 0; t < nTeams; t++) {
        // Get the slot in which team t plays.
        int s = (int) Math.round(cp.getValue(slot[t]));
        // Get the game and color for the slot.
        int xgame = problem.slotToGame(s);
        int xcolor = problem.slotToColor(s);
        // Insert it into the schedule.
        schedule.assign(t, xgame, xcolor);
      }
      return Optional.of(schedule);
    } else {
      // CPO does not have a solution, so return an empty schedule.
      return Optional.empty();
    }
  }

  /**
   * Sets a starting solution based on a known feasible schedule.
   * @param schedule the schedule from which to start
   * @throws IloException if the starting solution cannot be set
   */
  @Override
  public void setStart(final Schedule schedule) throws IloException {
    IloSolution sol = cp.solution();
    // Set the slot in which each team plays.
    for (int s = 0; s < nSlots; s++) {
      int t = schedule.getTeam(problem.slotToGame(s), problem.slotToColor(s));
      sol.setValue(slot[t], s);
    }
    // Set values for pod variables.
    for (int p = 0; p < nPods; p++) {
      // Get the colors worn by the pod.
      int[] colorsWorn = schedule.getColors(p);
      // Set values game by game.
      for (int g = 0; g < nGames; g++) {
        // Set the value for whether the pod is playing.
        int x = schedule.isPlaying(p, g) ? 1 : 0;
        sol.setValue(playing[p][g], x);
        // Set the jersey color.
        int c = colorsWorn[g];
        sol.setValue(color[p][g], c);
        // Set the value for the jersey change indicator.
        if (g == 0) {
          sol.setValue(change[p][g], 0);
        } else {
          int d = (colorsWorn[g] == colorsWorn[g - 1]) ? 0 : 1;
          sol.setValue(change[p][g], d);
        }
      }
    }
    // Set the opponent indicators.
    for (int p = 0; p < nPods; p++) {
      for (int q = p + 1; q < nPods; q++) {
        for (int g = 0; g < nGames; g++) {
          int o = schedule.areOpponents(p, q, g) ? 1 : 0;
          sol.setValue(opponents[p][q][g], o);
        }
      }
    }
    cp.setStartingPoint(sol);
    cp.startNewSearch();
    boolean success = cp.next();
    if (success) {
      System.out.println("Starting solution succeeded!");
    } else {
      System.out.println("Starting solution failed!");
      cp.refineConflict();
      cp.writeConflict();
    }
  }

  /**
   * Sets the solver time limit.
   * @param timeLimit the time limit in seconds
   * @throws IloException if CPO balks
   */
  @Override
  public void setTimeLimit(final double timeLimit) throws IloException {
    cp.setParameter(IloCP.DoubleParam.TimeLimit, timeLimit);
  }

  /**
   * Solves the model.
   * @return the final status (as a string)
   * @throws IloException if CPLEX encounters an error
   */
  @Override
  public String solve() throws IloException {
    // Optional: prioritize fixing the slot variables before anything else.
    IloSearchPhase phase = cp.searchPhase(slot);
    cp.setSearchPhases(phase);
    // Optional: change the search type.
    cp.setParameter(IloCP.IntParam.SearchType,
                    IloCP.ParameterValues.MultiPoint);
    cp.solve();
    return cp.getStatusString();
  }

}
