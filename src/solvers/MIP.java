package solvers;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import problem.Problem;
import problem.Schedule;

/**
 * MIP implements a MIP model for the tourney.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements Solver {
  private static final double HALF = 0.5;  // used to round values of binaries

  // Possible MIP emphasis values.
  private static final int EMPHASIS_OPTIMALITY = 2;
  private static final int EMPHASIS_BESTBOUND = 3;
  private static final int EMPHASIS_HEURISTIC = 5;

  private final Problem problem;  // parent problem
  private final int nPods;        // number of pods
  private final int nTeams;       // the number of teams
  private final int nSlots;       // number of schedule slots
  private final int nGames;       // number of games played

  // CPLEX objects.
  private final IloCplex mip;            // the solver instance
  private final IloIntVar[][] slot;      // slot[i][j] = 1 if team i
                                         // is in slot j
  private final IloIntVar[][][] oppose;  // oppose[i][j][s] = 1 if pod i is in
                                         // slot s and opposed by pod j
  private final IloIntVar[][] color;     // color[i][g] is the color (0 or 1)
                                         // warn by pod i after game g
  private final IloIntVar[][] change;    // change[i][g] = 1 if pod i changes
                                         // jersey color going into game g
  /**
   * Constructor.
   * @param prob the problem to solve
   * @throws IloException if CPLEX cannot build the model
   */
  public MIP(final Problem prob) throws IloException {
    problem = prob;
    nPods = problem.getNPods();
    nTeams = problem.getNTeams();
    nSlots = problem.getNSlots();
    nGames = problem.getNGames();
    // Initialize the solver.
    mip = new IloCplex();
    // Add the variables.
    slot = new IloIntVar[nTeams][nSlots];
    oppose = new IloIntVar[nPods][nPods][nSlots];
    color = new IloIntVar[nPods][nGames];
    change = new IloIntVar[nPods][nGames];
    for (int t = 0; t < nTeams; t++) {
      for (int s = 0; s < nSlots; s++) {
        slot[t][s] = mip.boolVar(problem.label("slot", "", t, s));
      }
    }
    // We only need to define the "oppose" variables for pods i and j
    // with i < j.
    for (int i = 0; i < nPods; i++) {
      for (int j = i + 1; j < nPods; j++) {
        for (int s = 0; s < nSlots; s++) {
          oppose[i][j][s] = mip.boolVar(problem.label("oppose", "", i, j, s));
        }
      }
    }
    for (int i = 0; i < nPods; i++) {
      for (int g = 0; g < nGames; g++) {
        color[i][g] = mip.boolVar(problem.label("color", "", i, g));
        change[i][g] = mip.boolVar(problem.label("change", "", i, g));
      }
    }
    // Add constraints.
    // Every team plays exactly once.
    for (int t = 0; t < nTeams; t++) {
      mip.addEq(mip.sum(slot[t]), 1.0, problem.label("team", "plays_once", t));
    }
    // Every schedule slot is filled exactly once.
    for (int s = 0; s < nSlots; s++) {
      IloLinearNumExpr expr = mip.linearNumExpr();
      for (int t = 0; t < nTeams; t++) {
        expr.addTerm(1.0, slot[t][s]);
      }
      mip.addEq(expr, 1.0, problem.label("fill", "once", s));
    }
    // Define the indicators for pods opposing each other variables.
    for (int i = 0; i < nPods; i++) {
      for (int j = i + 1; j < nPods; j++) {
        for (int s = 0; s < nSlots; s++) {
          int s1 = problem.oppositeSlot(s);
          IloLinearNumExpr sum1 = mip.linearNumExpr();
          IloLinearNumExpr sum2 = mip.linearNumExpr();
          // Sum indicators for teams containing pod i being in slot s.
          for (int t : problem.getTeams(i)) {
            sum1.addTerm(1.0, slot[t][s]);
          }
          // Sum indicators for teams containing pod j being in the slot
          // opposite to s.
          for (int t : problem.getTeams(j)) {
            sum2.addTerm(1.0, slot[t][s1]);
          }
          mip.addLe(oppose[i][j][s], sum1,
                    problem.label("oppose", "a", i, j, s));
          mip.addLe(oppose[i][j][s], sum2,
                    problem.label("oppose", "b", i, j, s));
          mip.addGe(oppose[i][j][s],
                    mip.diff(mip.sum(sum1, sum2), 1.0),
                    problem.label("oppose", "c", i, j, s));
        }
      }
    }
    // Every pair of pods opposes each other exactly twice.
    for (int i = 0; i < nPods; i++) {
      for (int j = i + 1; j < nPods; j++) {
        IloLinearNumExpr expr = mip.linearNumExpr();
        for (int s = 0; s < nSlots; s++) {
          expr.addTerm(1.0, oppose[i][j][s]);
        }
        mip.addEq(expr, 2.0, problem.label("pair", "plays_twice", i, j));
      }
    }
    // No pod plays in consecutive games in different color jerseys.
    for (int i = 0; i < nPods; i++) {
      // Iterate over all possible slots for the second of two games.
      for (int s = 1; s < nSlots; s++) {
        // Skip the black jersey slot of the first game.
        if (s != nGames) {
          int s1 = problem.oppositeSlot(s);
          IloLinearNumExpr sum = mip.linearNumExpr();
          for (int t : problem.getTeams(i)) {
            sum.addTerm(1.0, slot[t][s - 1]);
            sum.addTerm(1.0, slot[t][s1]);
          }
          mip.addLe(sum, 1.0, problem.label("jerseys", "", i, s));
        }
      }
    }
    // A pod playing in a game has its jersey color determined by its slot.
    // This has the side effect of preventing two teams containing a common pod
    // from playing against each other.
    for (int i = 0; i < nPods; i++) {
      Set<Integer> teams = problem.getTeams(i);
      for (int g = 0; g < nGames; g++) {
        // If playing in slot g, the jersey color is white (0).
        IloLinearNumExpr expr = mip.linearNumExpr(1.0);
        for (int t : teams) {
          expr.addTerm(-1.0, slot[t][g]);
        }
        mip.addLe(color[i][g], expr, problem.label("color", "white", i, g));
        // If playing in slot g + nGames, the jersey color is black.
        expr = mip.linearNumExpr(0.0);
        for (int t : teams) {
          expr.addTerm(1.0, slot[t][g + nGames]);
        }
        mip.addGe(color[i][g], expr, problem.label("color", "black", i, g));
      }
    }
    // A pod's color for any game (playing or not) is the same as its color
    // for the previous game, unless a change occurs.
    for (int i = 0; i < nPods; i++) {
      for (int g = 1; g < nGames; g++) {
        // Previous color 0 and no change implies new color 0.
        mip.addLe(color[i][g], mip.sum(color[i][g - 1], change[i][g]),
                  problem.label("stay_white", "", i, g));
        // Previous color 1 and no change implies new color 1.
        mip.addGe(color[i][g], mip.diff(color[i][g - 1], change[i][g]),
                  problem.label("stay_black", "", i, g));
      }
    }
    // New constraint (from the author of the original question):
    // No pod can play in more than MAXRUN straight games.
    for (int i = 0; i < nPods; i++) {
      // Get the teams containing pod i.
      Set<Integer> teams = problem.getTeams(i);
      // Create one constraint for each game that could be the
      // fourth consecutive.
      for (int g = Problem.MAXRUN; g < nGames; g++) {
        IloLinearNumExpr expr = mip.linearNumExpr();
        // Sum occurrences of any team containing pod i in either slot of any
        // of the four games culminating with game g.
        for (int g0 = g - Problem.MAXRUN; g0 <= g; g0++) {
          for (int t : teams) {
            expr.addTerm(1.0, slot[t][problem.getSlot(g0, 0)]);
            expr.addTerm(1.0, slot[t][problem.getSlot(g0, 1)]);
          }
        }
        // Limit the occurrences in those four games to at most 3.
        mip.addLe(expr, Problem.MAXRUN, problem.label("maxrun", "", i, g));
      }
    }
    // The objective is to minimize the number of jersey changes.
    IloLinearNumExpr obj = mip.linearNumExpr();
    for (int i = 0; i < nPods; i++) {
      for (int g = 0; g < nGames; g++) {
        obj.addTerm(1.0, change[i][g]);
      }
    }
    mip.addMinimize(obj);
  }

  /**
   * Gets the final schedule (if one exists).
   * @return the final schedule
   * @throws IloException if the schedule cannot be obtained
   */
  @Override
  public Optional<Schedule> getSchedule() throws IloException {
    Status status = mip.getStatus();
    if (status == Status.Optimal || status == Status.Feasible) {
      // CPLEX has a solution. Create a blank schedule.
      Schedule schedule = new Schedule(problem);
      // Translate each slot assignment into entries in the schedule.
      for (int t = 0; t < nTeams; t++) {
        for (int s = 0; s < nSlots; s++) {
          if (mip.getValue(slot[t][s]) > HALF) {
            // Team t is playing in slot s.
            int xgame = problem.slotToGame(s);
            int xcolor = problem.slotToColor(s);
            schedule.assign(t, xgame, xcolor);
          }
        }
      }
      return Optional.of(schedule);
    } else {
      // There is no solution, so return an empty schedule.
      return Optional.empty();
    }
  }

  /**
   * Sets a starting solution based on a known feasible schedule.
   * @param schedule the schedule from which to start
   * @throws IloException if the starting solution cannot be set
   */
  @Override
  public void setStart(final Schedule schedule) throws IloException {
    // Go through the schedule and make a list of slot variables that will
    // have value 1.
    ArrayList<IloIntVar> v = new ArrayList<>();
    for (int s = 0; s < nSlots; s++) {
      int t = schedule.getTeam(problem.slotToGame(s), problem.slotToColor(s));
      v.add(slot[t][s]);
    }
    IloNumVar[] vars = v.toArray(new IloNumVar[v.size()]);
    double[] vals = new double[vars.length];
    Arrays.fill(vals, 1.0);
    mip.addMIPStart(vars, vals);
  }

  /**
   * Solves the model.
   * @return the final status (as a string)
   * @throws IloException if CPLEX encounters an error
   */
  @Override
  public String solve() throws IloException {
    mip.setParam(IloCplex.IntParam.Emphasis.MIP, EMPHASIS_HEURISTIC);
//    mip.setParam(IloCplex.IntParam.Emphasis.MIP, EMPHASIS_OPTIMALITY);
    mip.solve();
    return mip.getStatus().toString();
  }

  /**
   * Sets the time limit for CPLEX.
   * @param timeLimit the time limit in seconds
   * @throws IloException if CPLEX balks
   */
  @Override
  public void setTimeLimit(final double timeLimit) throws IloException {
    mip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
  }

  /**
   * Closes the solver instance.
   */
  @Override
  public void close() {
    mip.end();
  }
}
