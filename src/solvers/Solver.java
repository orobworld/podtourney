package solvers;

import ilog.concert.IloException;
import java.util.Optional;
import problem.Schedule;

/**
 * Solve is an interface shared by all solvers.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public interface Solver extends AutoCloseable {

  /**
   * Sets a time limit for the solver.
   * @param timeLimit the solver time limit
   * @throws IloException if the solver balks
   */
  void setTimeLimit(final double timeLimit) throws IloException;

  /**
   * Solves the model.
   * @return the final status as a string
   * @throws IloException if the solver blows up
   */
  String solve() throws IloException;

  /**
   * Gets the schedule computed by the solver (if one exists).
   * @return the final schedule if it exists
   * @throws IloException if the schedule cannot be recovered
   */
  Optional<Schedule> getSchedule() throws IloException;

  /**
   * Closes the solver instance.
   */
  @Override
  void close();

  /**
   * Sets a starting solution based on a known feasible schedule.
   * @param schedule the schedule from which to start
   * @throws IloException if the starting solution cannot be set
   */
  void setStart(final Schedule schedule) throws IloException;
}
