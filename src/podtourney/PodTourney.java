package podtourney;

import ilog.concert.IloException;
import java.util.Optional;
import problem.Problem;
import problem.Schedule;
import solvers.CP;
import solvers.MIP;
import solvers.Solver;

/**
 * PodTourney uses a MIP model to solve a tournament scheduling question from
 * Mathematics Stack Exchange.
 *
 * https://math.stackexchange.com/questions/4370828/tournament-schedule
 * -assignment-via-convex-optimization
 *
 * A key assumption here is that exactly one game is played in each time slot
 * (no games in parallel, no time slots without a game).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class PodTourney {

  /**
   * Dummy constructor.
   */
  private PodTourney() { }

  /**
   * Solves the tournament instance.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // The problem dimensions were set in the Math SE post.
    int nPods = 9;                         // number of pods
    Schedule incumbent = null;             // best schedule so far
    // Set a time limit here (seconds).
    double timeLimit = 180;
    // Build the problem.
    Problem problem = new Problem(nPods);
    // Build and solve the MIP model.
    try (Solver mip = new MIP(problem)) {
      mip.setTimeLimit(timeLimit);
      String status = mip.solve();
      System.out.println("Final solver status = " + status);
      Optional<Schedule> schedule = mip.getSchedule();
      if (schedule.isPresent()) {
        incumbent = schedule.get();
        System.out.println(incumbent);
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // Build and solve the CP model.
    try (Solver cp = new CP(problem)) {
      timeLimit = 3600;
      cp.setTimeLimit(timeLimit);
      if (incumbent != null) {
        cp.setStart(incumbent);
      }
      String status = cp.solve();
      System.out.println("Final solver status = " + status);
      Optional<Schedule> schedule = cp.getSchedule();
      if (schedule.isPresent()) {
        incumbent = schedule.get();
        System.out.println(incumbent);
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
  }

}
